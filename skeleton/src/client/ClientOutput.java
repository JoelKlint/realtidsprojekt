package client;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Thread that writes data to an Axis camera
 */
public class ClientOutput extends Thread{

	private ClientMonitor monitor;
	private OutputStream outStream;

	/**
	 * Creates the thread
	 * @param outStream OutputStream to read from
	 * @param monitor Monitor to communicate the data from
	 */
	public ClientOutput(OutputStream outStream, ClientMonitor monitor) {
		this.outStream = outStream;
		this.monitor = monitor;
	}

	/**
	 * Sends currently set surveillance modes to Server.
	 */
	@Override
	public void run() {
		while (true) {
			try {
				outStream.write(monitor.detectNewSurvMode());
			} catch (IOException e) {
				// TODO Take care of exception
				e.printStackTrace();
			}
		}
	}

}
