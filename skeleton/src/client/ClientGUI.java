package client;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import globals.ManualMode;
import globals.Picture;
import globals.SurveillanceMode;
/**
 * Generates a gui for 2 cameras.
 *
 */
public class ClientGUI extends JFrame {

	ClientImagePanel imagePanel;
	JPanel buttonPanel;
	ButtonGroup surveillanceModeButtonGroup, syncModeButtonGroup;
	JRadioButton idleModeButton, movieModeButton, syncModeButton, asyncModeButton;
	JButton connectButton, manualButton;
	static final String AutoModeText = "Auto Mode", ManualModeText = "Manual Mode";
	static final String ConnectText = "Connect", DisconnectText = "Disconnect";
	byte cameraMotionDetect;
	boolean firstCall = true;

	public ClientGUI() {
		super();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Image + delay
		imagePanel = new ClientImagePanel(new GridBagLayout());
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(imagePanel, BorderLayout.NORTH);

		// Buttons
		buttonPanel = new JPanel(new FlowLayout());

		// Surveillance buttons
		idleModeButton = new JRadioButton("Idle Mode");
		idleModeButton.addActionListener(new GuiButtonHandler(this));
		idleModeButton.setActionCommand("idle");
		movieModeButton = new JRadioButton("Movie Mode");
		movieModeButton.addActionListener(new GuiButtonHandler(this));
		movieModeButton.setActionCommand("movie");

		surveillanceModeButtonGroup = new ButtonGroup();
		surveillanceModeButtonGroup.add(idleModeButton);
		surveillanceModeButtonGroup.add(movieModeButton);
		idleModeButton.setSelected(true);

		// Sync buttons
		syncModeButton = new JRadioButton("Sync Mode");
		syncModeButton.addActionListener(new GuiButtonHandler(this));
		syncModeButton.setActionCommand("sync");
		asyncModeButton = new JRadioButton("Ascync Mode");
		asyncModeButton.addActionListener(new GuiButtonHandler(this));
		asyncModeButton.setActionCommand("async");

		syncModeButtonGroup = new ButtonGroup();
		syncModeButtonGroup.add(syncModeButton);
		syncModeButtonGroup.add(asyncModeButton);
		syncModeButton.setSelected(true);

		// Manual Mode button
		manualButton = new JButton(AutoModeText);
		manualButton.addActionListener(new GuiButtonHandler(this));
		manualButton.setActionCommand("manual");

		// Add buttons to panel
		buttonPanel.add(idleModeButton);
		buttonPanel.add(movieModeButton);
		buttonPanel.add(syncModeButton);
		buttonPanel.add(asyncModeButton);
		buttonPanel.add(manualButton);

		this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		this.setLocationRelativeTo(null);
		this.pack();
		setResizable(false);
		setVisible(true);
	}

	public void setLeftPicture(Picture pic) {
		
		if(pic.getMotionDetect() == 1){
			cameraMotionDetect = 1;
		}
		
		imagePanel.refreshLeft(pic.getPicture(), pic.getDelay(), cameraMotionDetect);

		this.pack();

		revalidate();
		repaint();
	}

	public void setRightPicture(Picture pic) {
		
		if(pic.getMotionDetect() == 1){
			cameraMotionDetect = 2;
		}
		
		imagePanel.refreshRight(pic.getPicture(), pic.getDelay(), cameraMotionDetect);

		this.pack();

		revalidate();
		repaint();
	}

	public void enterManualMode() {
		ClientMain.MANUALMODE = ManualMode.TRUE;
		manualButton.setText(ManualModeText);
	}

	public void setMovieMode() {
		if (!movieModeButton.isSelected()) {
			movieModeButton.setSelected(true);
		}
	}

	public void setSyncMode(int mode) {
		if (mode == SyncMode.SYNCHRONIZED) {
			syncModeButton.setSelected(true);
		} else if (mode == SyncMode.ASYNCHRONIZED) {
			asyncModeButton.setSelected(true);
		}
	}
}
/**
 * Implements all button features.
 * 
 *
 */
class GuiButtonHandler implements ActionListener {
	ClientGUI gui;

	public GuiButtonHandler(ClientGUI clientGUI) {
		this.gui = clientGUI;
	}

	public void actionPerformed(ActionEvent evt) {
		switch (evt.getActionCommand()) {
		case "idle":
			ClientMain.SURVEILLANCEMODE = SurveillanceMode.IDLE;
			System.out.println("Idle button pressed");
			gui.enterManualMode();
			gui.cameraMotionDetect = 0;
			break;
		case "movie":
			ClientMain.SURVEILLANCEMODE = SurveillanceMode.MOVIE;
			System.out.println("Movie button pressed");
			gui.enterManualMode();
			break;
		case "sync":
			ClientMain.SYNCHRONIZATIONMODE = SyncMode.SYNCHRONIZED;
			System.out.println("Sync button pressed");
			gui.enterManualMode();
			break;
		case "async":
			ClientMain.SYNCHRONIZATIONMODE = SyncMode.ASYNCHRONIZED;
			System.out.println("Async button pressed");
			gui.enterManualMode();
			break;
		case "manual":
			String manualButtonText = gui.manualButton.getText();
			if (manualButtonText.equals(ClientGUI.AutoModeText)) {
				ClientMain.MANUALMODE = ManualMode.TRUE;
				gui.manualButton.setText(ClientGUI.ManualModeText);
			} else if (manualButtonText.equals(ClientGUI.ManualModeText)) {
				ClientMain.MANUALMODE = ManualMode.FALSE;
				gui.manualButton.setText(ClientGUI.AutoModeText);
			}
			System.out.println("Manual button pressed");
			break;
		default:
			System.out.println("Unknown button pressed");
			break;
		}

	}

}
/**
 * Part of gui.
 * 
 *
 */
class ClientImagePanel extends JPanel {
	ImageIcon icon1, icon2;
	JLabel delayLabel1, delayLabel2, motionDetectLeftLabel, motionDetectRightLabel;
	int cameraMotionDetect;

	public ClientImagePanel(GridBagLayout gridBagLayout) {
		super(gridBagLayout);

		GridBagConstraints c = new GridBagConstraints();

		c.fill = GridBagConstraints.HORIZONTAL; // Fills width
		c.weightx = 0.5; // TODO: What does this do exactly? Only needed when
							// resizable?

		icon1 = new ImageIcon();
		JLabel label = new JLabel(icon1);
		c.gridx = 0;
		c.gridy = 0;
		this.add(label, c);

		icon2 = new ImageIcon();
		JLabel label2 = new JLabel(icon2);
		c.gridx = 1;
		c.gridy = 0;
		this.add(label2, c);

		delayLabel1 = new JLabel("Delay1");
		c.gridx = 0;
		c.gridy = 1;
		c.ipady = 10;
		c.insets = new Insets(0, 300, 0, 0); // TODO: Change 400 to half the
												// size - e.g. 20 (if necessary)
		this.add(delayLabel1, c);

		delayLabel2 = new JLabel("Delay2");
		c.gridx = 1;
		c.gridy = 1;
		this.add(delayLabel2, c);

		motionDetectLeftLabel = new JLabel();
		c.gridx = 0;
		c.gridy = 2;
		this.add(motionDetectLeftLabel, c);

		motionDetectRightLabel = new JLabel();
		c.gridx = 1;
		c.gridy = 2;
		this.add(motionDetectRightLabel, c);
	}
	/**
	 * Updates the left camera picture.
	 * @param data
	 * @param delay
	 * @param cameraMotionDetect
	 */
	public void refreshLeft(byte[] data, float delay, byte cameraMotionDetect) {
		Image theImage = getToolkit().createImage(data);
		getToolkit().prepareImage(theImage, -1, -1, null);
		icon1.setImage(theImage);
		switch (cameraMotionDetect) {
		case 0:
			motionDetectLeftLabel.setText("");
			break;
		case 1:
			motionDetectLeftLabel.setText("Motion Detected");
			break;
		}

		delayLabel1.setText("Delay " + Float.toString(delay) + " ms");
	}

	/**
	 * Updates the right camera picture.
	 * @param data
	 * @param delay
	 * @param cameraMotionDetect
	 */
	public void refreshRight(byte[] data, float delay, byte cameraMotionDetect) {
		Image theImage = getToolkit().createImage(data);
		getToolkit().prepareImage(theImage, -1, -1, null);
		icon2.setImage(theImage);
		switch (cameraMotionDetect) {
		case 0:
			motionDetectRightLabel.setText("");
			break;
		case 2:
			motionDetectRightLabel.setText("Motion Detected");
			break;
		}

		delayLabel2.setText("Delay " + Float.toString(delay) + " ms");
	}
}