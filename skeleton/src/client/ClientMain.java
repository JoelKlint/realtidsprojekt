package client;

import globals.ManualMode;
import globals.SurveillanceMode;

/**
 * Class for easily starting a Client in an Axis camera ecosystem
 */
public class ClientMain {
	
	public static volatile int SURVEILLANCEMODE = SurveillanceMode.IDLE;
	public static volatile int SYNCHRONIZATIONMODE = SyncMode.SYNCHRONIZED;
	public static volatile int MANUALMODE = ManualMode.FALSE; 
	/**
	 * Threshold in milliseconds deciding when system should enter asynchronous mode in 
	 */
	public static final double SYNCHRONIZATIONTHRESHOLD = 200;


	public static void main(String[] args) {

		String serverAddress1 = "localhost";
		int portNumber1 = Integer.parseInt(args[0]);
		
		String serverAddress2 = "localhost";
		int portNumber2 = Integer.parseInt(args[1]);

		ClientGUI gui = new ClientGUI();
		ClientMonitor[] monitors = new ClientMonitor[2];
		monitors[0] = new ClientMonitor();
		monitors[1] = new ClientMonitor();
		
		new ClientConnector(serverAddress1, portNumber1, monitors[0], gui);
		new ClientConnector(serverAddress2, portNumber2, monitors[1], gui);
		
		new PictureFetcher(monitors, gui).start();
		
	}
	
	/**
	 * Method for setting the global surveillance mode of the ecosystem
	 * @param newMode newMode to set. Use constants from class SurveillanceMode.
	 */
	public static void setSurveillanceMode(int newMode)	{
		SURVEILLANCEMODE = newMode;
	}

}
