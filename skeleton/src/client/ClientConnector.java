package client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Handles connection to an Axis camera
 *
 */
public class ClientConnector {	
	
	/**
	 * Connects to the camera and start input and output threads
	 * @param address Address where the camera can be reached
	 * @param portNumber Internet port you wish to use 
	 * @param monitor Monitor to store all pictures in
	 */
	public ClientConnector(String address, int portNumber,  ClientMonitor monitor, ClientGUI gui)	{
//		Declare necessary variables
		System.out.println("Client trying to connect to \"" + address + "\"");
		Socket socket = null;
		InputStream inStream = null;
		OutputStream outStream = null;

//		Connect to the camera
		try {
			System.out.println("Trying to connect to Server");
			socket = new Socket(address, portNumber);
			System.out.println("Client Socket running");
			inStream = socket.getInputStream();
			outStream = socket.getOutputStream();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("Ready to Recivie");
		
//		Start Input and Output threads
		new ClientInput(inStream, monitor, gui).start();
		new ClientOutput(outStream, monitor).start();
	}

}
