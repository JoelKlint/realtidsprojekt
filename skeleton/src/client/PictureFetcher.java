package client;

import javax.swing.SwingUtilities;

import globals.ManualMode;
import globals.Picture;
import globals.SurveillanceMode;

/**
 * Thread for extracting pictures from a ClientMonitor and sending it to a
 * ClientGUI
 */
public class PictureFetcher extends Thread {

	private ClientMonitor[] monitors;
	private ClientGUI gui;

	/**
	 * Instantiates the PictureFetcher Thread.
	 * 
	 * @param monitors
	 *            Monitor to fetch pictures from
	 * @param gui
	 *            GUI to send pictures to
	 */
	public PictureFetcher(ClientMonitor[] monitors, ClientGUI gui) {
		this.monitors = monitors;
		this.gui = gui;
	}
	/**
	 * Determines which mode the system should be in and sends pictures to the gui accordingly.
	 */
	@Override
	public void run() {
		while (true) {
			// Get pictures
			final Picture leftPic = monitors[0].fetchPicture();
			final Picture rightPic = monitors[1].fetchPicture();

			//Calculate current mode
			long delayDiff = Math.abs(leftPic.getDelay() - rightPic.getDelay());
			if (delayDiff > ClientMain.SYNCHRONIZATIONTHRESHOLD && ClientMain.MANUALMODE == ManualMode.FALSE) {
				ClientMain.SYNCHRONIZATIONMODE = SyncMode.ASYNCHRONIZED;
			} else if (delayDiff < ClientMain.SYNCHRONIZATIONTHRESHOLD && ClientMain.MANUALMODE == ManualMode.FALSE) {
				ClientMain.SYNCHRONIZATIONMODE = SyncMode.SYNCHRONIZED;
			}

			//If synchronized
			if (ClientMain.SYNCHRONIZATIONMODE == SyncMode.SYNCHRONIZED) {
				sendPicturesToGUISynchronized(leftPic, rightPic);
			}
			//If not Synchronized
			else if (ClientMain.SYNCHRONIZATIONMODE == SyncMode.ASYNCHRONIZED) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						gui.setLeftPicture(leftPic);
						gui.setRightPicture(rightPic);
					}
				});
			}
		}
	}


	/**
	 * Find the oldest of the two pictures and sends it to GUI. Then wait the
	 * difference between capture time before it displays the other one
	 * 
	 * @param leftPic
	 *            The picture that should be shown on the left in GUI
	 * @param rightPic
	 *            The picture that should be shown on the right in GUI
	 */
	private void sendPicturesToGUISynchronized(final Picture leftPic, final Picture rightPic) {
		// positive if pic1 is older. Negative if pic2 is older
		final long diff = leftPic.getTimeStamp() - rightPic.getTimeStamp();
		if (diff > 0) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					gui.setLeftPicture(leftPic);
				}
			});
			// Wait the difference between capture times
			try {
				Thread.sleep(Math.abs(diff));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					gui.setRightPicture(rightPic);
				}
			});

		} else {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					gui.setRightPicture(rightPic);
				}
			});
			// Wait the difference between capture times
			try {
				Thread.sleep(Math.abs(diff));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					gui.setLeftPicture(leftPic);
				}
			});
		}
	}
}
