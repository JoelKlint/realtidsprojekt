package client;

import java.io.IOException;
import java.io.InputStream;

import globals.IpPackage;
import globals.ManualMode;
import globals.Picture;
import globals.SurveillanceMode;

/**
 * Thread that reads data from an Axis camera
 *
 */
public class ClientInput extends Thread {

	private ClientMonitor monitor;
	private InputStream inStream;
	int i;
	private ClientGUI gui;

	/**
	 * Creates the thread
	 * 
	 * @param inStream
	 *            InputStream to read from
	 * @param monitor
	 *            Monitor to communicate the data to
	 * @param gui
	 * 			  Gui to show pictures in.
	 * 
	 */
	public ClientInput(InputStream inStream, ClientMonitor monitor, ClientGUI gui) {
		this.inStream = inStream;
		this.monitor = monitor;
		this.gui = gui;
	}

	@Override
	public void run() {
		while (true) {
			// Read image
			byte[] pictureData;
			long timeStamp;
			try {
				
				
				// Read package size
				int lengthToRead = IpPackage.PACKAGELENGTH;
				byte[] readBytes = new byte[lengthToRead];
				int read = 0;
				int result = 0;
				while(read<lengthToRead && result != -1)	{
					result = inStream.read(readBytes, read, lengthToRead - read);
					if(result != -1)	{
						read = read + result;
					}
				}
				int pictureSize = IpPackage.bytesToInt(readBytes);
				
				// Read picure data
				lengthToRead = pictureSize;
				pictureData = new byte[lengthToRead];
				read = 0;
				result = 0;				
				while(read<lengthToRead && result != -1)	{
					result = inStream.read(pictureData, read, lengthToRead - read);
					if(result != -1)	{
						read = read + result;
					}
				}
				
				// Read timestamp data
				lengthToRead = IpPackage.TIMESTAMPLENGTH;
				readBytes = new byte[lengthToRead];
				read = 0;
				result = 0;
				while(read<lengthToRead && result != -1)	{
					result = inStream.read(readBytes, read, lengthToRead - read);
					if(result != -1)	{
						read = read + result;
					}
				}
				timeStamp = IpPackage.bytesToLong(readBytes);
				
				//Calculate diff
				long delay = System.currentTimeMillis() - timeStamp;
				
				// Read motion detected data and act on it
				byte motionDetected = (byte) inStream.read();
				if(motionDetected == 1 && ClientMain.MANUALMODE == ManualMode.FALSE)	{
					ClientMain.SURVEILLANCEMODE = SurveillanceMode.MOVIE;
					gui.setMovieMode();
				}
				
				
				// Create Picture object and add to monitor
				Picture picture = new Picture(pictureData, delay, timeStamp, motionDetected);
				monitor.addPicture(picture);
				
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}

	}
}
