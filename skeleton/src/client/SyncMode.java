package client;

/**
 * Enum for easily handling Synchronization modes for a client managing several Axis cameras.
 *
 */
public class SyncMode {
	/**
	 * int representation for synchronized mode.
	 */
	public static final int SYNCHRONIZED = 10;
	/**
	 * int representation for asynchronized mode.
	 */
	public static final int ASYNCHRONIZED = 20;

}
