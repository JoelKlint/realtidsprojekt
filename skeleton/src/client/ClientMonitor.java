package client;

import globals.Picture;

/**
 * Monitor that handles all data for an Axis camera
 */
public class ClientMonitor {
	private Picture p;

	/**
	 * Creates the monitor
	 */
	public ClientMonitor() {
		
	}

	/**
	 * A method that blocks until a new SurveillanceMode has been detected
	 * 
	 * @return the new SurveillanceMode
	 */
	public synchronized int detectNewSurvMode() {
		int oldSurveillanceMode = ClientMain.SURVEILLANCEMODE;
		while (ClientMain.SURVEILLANCEMODE == oldSurveillanceMode)
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Take care of exception
				e.printStackTrace();
			}
		return ClientMain.SURVEILLANCEMODE;
	}

	/**
	 * Stores a picture in the monitor
	 * 
	 * @param picture
	 *            Picture to be stored
	 */
	public synchronized void addPicture(Picture picture) {
		p = picture;
		notifyAll();
	}

	/**
	 * Fetches currently stored image and removes it. Blocking 
	 * @return currently stored image.
	 */
	public synchronized Picture fetchPicture() {
		while(p == null)	{
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Picture toSend = p;
		p = null;
		notifyAll();
		return toSend;
	}
	


}
