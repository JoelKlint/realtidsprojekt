package main;

import java.io.IOException;

import client.ClientMain;
import server.ServerDriver;

public class proxyMain {
	
	static String ad1, ad2, p1, p2;

	/**
	 * Starts everything
	 * @param args
	 */
	public static void main(String[] args) {
		ad1 = args[0];
		ad2 = args[2];
		p1 = args[1];
		p2 = args[3];
		
		Server1 s1 = new Server1();
		s1.start();
		Server2 s2 = new Server2();
		s2.start();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Client c = new Client();
		c.start();
		

	}
	
	/**
	 * Starts client
	 * 
	 *
	 */
	private static class Client extends Thread {
		public void run() {
//			String[] input = new String[4];
//			input[0] = "localhost";
//			input[1] = "4141";
//			input[2] = "localhost";
//			input[3] = "4242";
			ClientMain.main(new String[]{p1, p2}); // TODO: Set up a similar structure as to Demo.
		}
	}
	/**
	 * Starts Server1
	 * 
	 *
	 */
	private static class Server1 extends Thread{
		public void run(){
			try {
//				String[] input = new String[2];
//				input[0] = "argus-1.student.lth.se";
//				input[1] = "4141";
				ServerDriver.main(new String[]	{ad1, p1});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Starts Server2
	 * 
	 *
	 */
	private static class Server2 extends Thread{
		public void run(){
			try {
//				String[] input = new String[2];
//				input[0] = "argus-8.student.lth.se";
//				input[1] = "4242";
				ServerDriver.main(new String[]	{ad2, p2});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
