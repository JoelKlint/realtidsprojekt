package globals;

/**
 * Class for easily representing Surveillance Modes in an Axis camera ecosystem.
 * 
 *
 */
public class SurveillanceMode {
	
	/**
	 * int representation for mode MOVIE
	 */
	public static final int MOVIE = 1;
	
	/**
	 * int representation for mode IDLE
	 */
	public static final int IDLE = 0;
}
