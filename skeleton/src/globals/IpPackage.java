package globals;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Describes an IP-package between an Axis camera and the connected client
 */
public class IpPackage {

	/**
	 * The number of bytes that holds the package size
	 */
	
	public static final int PACKAGELENGTH = 4;
	
	/**
	 * The number of bytes that holds the timestamp.
	 */	
	public static final int TIMESTAMPLENGTH = 8;
	
	/**
	 * The number of bytes that hold the motion detected signal
	 */
	public static final int MOTIONDETECTEDLENGTH = 1;

	/**
	 * Translates a binary number an int. 
	 * @param bytes holds the binary number. One bit per index 
	 * @return the int representation 
	 */
	public static int bytesToInt(byte[] bytes) {
		return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getInt();
	}
	
	/**
	 * Translates a binary number a long. 
	 * @param bytes holds the binary number. One bit per index 
	 * @return the int representation 
	 */
	public static long bytesToLong(byte[] bytes) {
		return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getLong();
	}

}
