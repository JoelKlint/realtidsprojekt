package globals;
/**
 * Representing the states if the system is in manual or automode. 
 */
public class ManualMode {

	/**
	 * int representation for mode Manual button not pressed
	 */
	public static final int FALSE = 0;

	/**
	 * int representation for mode Manual button pressed
	 */
	public static final int TRUE = 1;
}
