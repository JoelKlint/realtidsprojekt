package globals;

/**
 * Represents a picture
 * 
 *
 */
public class Picture {
	
	private byte[] pictureData; 
	private long delay;
	private long timeStamp;
	private byte motionDetect;
	private byte[] timeStampBytes;
	
/**
 * Creates a new picture. This is a constructor for ClientSide
 * @param picture
 * @param delay
 * @param timeStamp
 * @param motionDetect
 */
	public Picture(byte[] picture, long delay, long timeStamp, byte motionDetect)	{
		this.pictureData = picture;
		this.delay = delay;	
		this.timeStamp = timeStamp;
		this.motionDetect = motionDetect;
	}
	
/**
 * Creates a new picture. This is a constructor for ServerSide
 * @param picture
 * @param timeStampBytes
 */
	public Picture(byte[] picture, byte[] timeStampBytes)	{
		this.pictureData = picture;
		this.timeStampBytes = timeStampBytes;		
	}
	
	/**
	 * Returns the picture data 
	 * @return the picture data held by this Picture
	 */
	public byte[] getPicture() {
		return pictureData;
	}

	/**
	 * Return the time it took to transfer the image from the server to the client
	 * @return the delay time
	 */
	public long getDelay() {
		return Math.abs(delay);
	}
	
	/**
	 * Returns the motion detect value
	 * @return motion detect value
	 */
	public byte getMotionDetect()	{
		return motionDetect;
	}
	
	/**
	 * Returns the time this picture was takes
	 * @return the time this picture was taken
	 */
	public long getTimeStamp()	{
		return timeStamp;
	}
	
	/**
	 * Return the time this picture was takes
	 * @return the time this picture was takes
	 */
	public byte[] getTimeStampBytes(){
		return timeStampBytes;
	}

}
