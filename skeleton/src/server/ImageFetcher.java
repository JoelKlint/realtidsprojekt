package server;

import globals.SurveillanceMode;
//import se.lth.cs.eda040.fakecamera.*;
import se.lth.cs.eda040.proxycamera.AxisM3006V;
/**
 * 
 * Responsible for getting pictures from camera.
 *
 */
public class ImageFetcher extends Thread {
	private ServerMonitor monitor;
	private AxisM3006V camera;

	/**
	 * This thread tries to get images from a camera via a specific port, and
	 * put it into the monitor
	 * 
	 * @param monitor
	 * @param port
	 */
	public ImageFetcher(ServerMonitor monitor, String address, int port) {
		this.monitor = monitor;
		camera = new AxisM3006V();
		camera.init();
		camera.setProxy(address, port);
		camera.connect();
	}

	/**
	 * Gets an image and timestamp from the camera and returns it to the
	 * monitor.
	 */
	public void run() {
		while (true) {
			if (true) {
				if (monitor.getMode() == SurveillanceMode.MOVIE) {
					byte[] picture = new byte[camera.IMAGE_BUFFER_SIZE];
					int pictureValue = camera.getJPEG(picture, 0);
					byte[] timeStamp = new byte[8];
					boolean motionDetected = camera.motionDetected();
					if(motionDetected){
						monitor.setMotionDetected((byte) 1);
					}else{
						monitor.setMotionDetected((byte) 0);
					}
					camera.getTime(timeStamp, 0);
					monitor.putPicture(picture, timeStamp, pictureValue);
				} else {
					try {
						byte[] picture = new byte[camera.IMAGE_BUFFER_SIZE];
						int pictureValue = camera.getJPEG(picture, 0);
						byte[] timeStamp = new byte[8];
						camera.getTime(timeStamp, 0);
						boolean motionDetected = camera.motionDetected();
						if(motionDetected){
							monitor.setMotionDetected((byte) 1);
						}else{
							monitor.setMotionDetected((byte) 0);
						}
						monitor.putPicture(picture, timeStamp, pictureValue);
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}
	}
}
