package server;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerDriver {
	/**
	 * Initiates the server and starts important Threads
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String address = args[0];
		int port = Integer.parseInt(args[1]);
		ServerSocket ss = new ServerSocket(port);
		ServerMonitor monitor = new ServerMonitor();
		ImageFetcher imageFetcher = new ImageFetcher(monitor, address, port);
		ServerConnector sc = new ServerConnector(monitor, ss);
		imageFetcher.start();
	}

}
