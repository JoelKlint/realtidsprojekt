package server;

import java.io.IOException;
import java.io.OutputStream;

import globals.Picture;
/**
 * 
 * Is responsible for sending images to the client.
 *
 */
public class ServerOutput extends Thread {
	private ServerMonitor monitor;
	private OutputStream os;
	/**
	 * Initiates a new Server Output thread.
	 * @param monitor
	 * @param os
	 */
	public ServerOutput(ServerMonitor monitor, OutputStream os) {
		this.monitor = monitor;
		this.os = os;

	}
	/**
	 * Recives image from monitor and creates a package to send to the client.
	 */
	public void run() {
		while (true) {
			try {
				Picture picture = monitor.fetchPicture();
				byte[] jpeg = compressPictureSize(picture.getPicture());
				int pictureSize = jpeg.length;

				// packet size + picture size + timestamp + motiondetect
				byte[] packet = new byte[4 + pictureSize + 8 + 1];
				byte[] jpegSizeBytes = intToBytes(jpeg.length);
				byte motionDetect = monitor.getMotionDetected();
				int j = 0;
				for (int i = 0; i < packet.length; i++) {
					if (i < 4) {
						packet[i] = jpegSizeBytes[i];
					} else if (i >= 4 && i < pictureSize + 4) {
						packet[i] = jpeg[i - 4];
					} else if (i >= 4 + pictureSize && i < packet.length - 1) {
						packet[i] = picture.getTimeStampBytes()[j++];
					} else {
						packet[i] = motionDetect;
					}
				}
				os.write(packet);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	private byte[] intToBytes(int i) {
		byte[] array = new byte[4];
		int index = 0;
		array[index++] = (byte) ((i & 0xff000000L) >> 24);
		array[index++] = (byte) ((i & 0x00ff0000L) >> 16);
		array[index++] = (byte) ((i & 0x0000ff00L) >> 8);
		array[index++] = (byte) ((i & 0x000000ffL));
		return array;
	}

	private byte[] compressPictureSize(byte[] picture) {
		int size = 0;
		int endOfPic = 0;
		byte[] compressed = null;
		for (byte b : picture) {
			if (endOfPic < 20) {
				size++;
				if (b == 0)
					endOfPic++;
				else
					endOfPic = 0;
			}
		}
		size = size - 20;
		compressed = new byte[size];
		for (int i = 0; i < size; i++) {
			compressed[i] = picture[i];
		}
		return compressed;
	}
}
