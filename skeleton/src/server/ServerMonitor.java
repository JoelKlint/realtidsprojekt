package server;

import globals.Picture;
import globals.SurveillanceMode;

public class ServerMonitor {
	private byte mode;
	private Picture latestPic;
	private boolean pictureUpdated;
	private byte motionDetected;
	/**
	 * Monitor containing all resources for the server
	 */
	public ServerMonitor(){
		mode = SurveillanceMode.IDLE;
	}
	/**
	 * Tries to put a picture into the monitor.
	 * Waits while no new picture has been received.
	 * @param picture
	 * @param timeStampBytes
	 * @param pictureValue
	 */
	public synchronized void putPicture(byte[] picture, byte[] timeStampBytes, int pictureValue){
		while(pictureValue == 0)
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		latestPic = new Picture(picture, timeStampBytes);
		pictureUpdated = true;
		notifyAll();
	}
	
	/**
	 * Tries to get the picture from the monitor.
	 * Waits while there is no picture or the picture in the monitor is the same as the latest fetched.
	 * @return (Picture) picture in monitor
	 */
	public synchronized Picture fetchPicture(){
		while(latestPic == null || pictureUpdated == false){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		pictureUpdated = false;
		notifyAll();
		return latestPic;
	}
	
	/**
	 * Changes the servers currently running mode.
	 * Waits while no new mode is received.
	 * @param mode
	 */
	public synchronized void changeMode(byte mode){
		this.mode = mode;
		notifyAll();
	}
	/**
	 * Returns the current mode
	 * @return current mode
	 */
	public synchronized byte getMode(){
		return mode;
	}
	
	/**
	 * Sets if motion has been detected.
	 * @param motion
	 */
	public synchronized void setMotionDetected(byte motion){
		motionDetected = motion;
	}
	
	/**
	 * Check if any motion has been detected.
	 * @return current motion detected state.
	 */
	public synchronized byte getMotionDetected(){
		return motionDetected;
	}
	
	
}
