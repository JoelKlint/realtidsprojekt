package server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Tries to connect the server with a client through a socket. 
 * Also starts the server input and output threads
 */
public class ServerConnector {

	/**
	 * Initiates a Connector for server input and output
	 * @param monitor
	 * @param ss
	 * @throws IOException
	 */
	public ServerConnector(ServerMonitor monitor, ServerSocket ss) throws IOException	{
		Socket socket = ss.accept();
		InputStream inStream = null;
		OutputStream outStream = null;
		
		try {
			System.out.println("Waiting for client to connect");
			socket.setTcpNoDelay(true);
			System.out.println("Server Socket running");
			// TODO Change portNumber typ to int and parse in main
			inStream = socket.getInputStream();
			outStream = socket.getOutputStream();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("Server ready to Recivie");
		ServerInput input = new ServerInput(monitor, inStream);
		ServerOutput output = new ServerOutput(monitor, outStream);
		input.start();
		output.start();
	}

}
