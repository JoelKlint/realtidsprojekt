package server;
import java.io.InputStream;

import java.io.IOException;

/**
 * Handles all input from client.
 *
 */
public class ServerInput extends Thread {
	private ServerMonitor monitor;
	private InputStream is;

	/**
	 * Creates a server input thread. This thread waits for mode changes coming from Client
	 * @param monitor
	 * @param is
	 */
	public ServerInput(ServerMonitor monitor, InputStream is) {
		this.monitor = monitor;
		this.is = is;
	}
	/**
	 * Updates the current mode in the monitor based on the client output
	 */
	public void run() {
		while (true) {
			try {
				byte[] surveillanceMode = new byte[1];
				is.read(surveillanceMode);
				monitor.changeMode(surveillanceMode[0]);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
